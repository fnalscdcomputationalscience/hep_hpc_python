---
title: "Looking at LArIAT ADC data"
author: "Marc Paterno"
date: "2/7/2018"
output: html_document
---

```{r setup, include=FALSE}
library(latticeExtra)
library(dplyr)
library(magrittr)
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

We have single HDF5 file containing the raw ADC data of 48,457 events. We process this file using an python/MPI program to produced filtered ADC data, using the FFT technique as does the CalROIT1034 module.

Each event contains `u` and `v` wires, 240 of each, with 3072 samples per wire. For the first 100 events, we wrote out both the _raw_ and the _filtered_ ADC data, for _interesting_ wires. We called a wire _interesting_ if any of the samples has an absolute value greater than 50. There were 257 wires in the 100 events processed.

## Reading the data

We read the data into matrices, for easy manipulation.

```{r, cache = TRUE}
raw <- scan("raw.txt", what = numeric(), sep = " ")
raw <- matrix(raw, ncol = 3072, byrow = TRUE)
dimnames(raw) <- NULL
wave <- scan("wave.txt", what = numeric(), sep = " ")
wave <- matrix(wave, ncol = 3072, byrow = TRUE)
dimnames(wave) <- NULL
str(raw)
str(wave)
```

```{r}
plot(raw[8,], pch = ".")
plot(wave[8,], pch = ".")
```


