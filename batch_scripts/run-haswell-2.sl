#!/bin/bash -l
#SBATCH --account=m1523
#SBATCH -p debug
#SBATCH -N 2
#SBATCH -t 00:30:00
#SBATCH -J my_job
#SBATCH -o hist_haswell_64_2_1.o 
#SBATCH -L SCRATCH
#SBATCH -C haswell


module unload python
module load python/2.7-anaconda
module load h5py-parallel/2.7.1

srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 0
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 1
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 2
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 3
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 4
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 5
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 6
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 7
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 8
srun -n 64 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 9

sed -i '1s/^/Nodes Cores Label Time Platform Memmode Run\n/' hist_haswell_64_2_1.o 
