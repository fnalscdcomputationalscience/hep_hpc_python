#!/bin/bash -l
#SBATCH --account=m2696
#SBATCH -p debug
#SBATCH -N 32
#SBATCH -t 00:30:00
#SBATCH -J cms_ana_haswell
#SBATCH -L SCRATCH
#SBATCH -C haswell


module unload python
module load python/2.7-anaconda
module load h5py-parallel/2.7.1

srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 0 $2 $3 $4 
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 1 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 2 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 3 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 4 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 5 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 6 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 7 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 8 $2 $3 $4
srun -n 1024 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 9 $2 $3 $4

#sed -i '1s/^/Myrank, Nodes, Cores, Tasks, Timestamp, Time, Platform, Memmode, Run, Batch\n/' $1 
