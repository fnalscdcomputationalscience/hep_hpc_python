#!/bin/bash -l
#SBATCH --account=m2696
#SBATCH -p debug
#SBATCH -N 4
#SBATCH -t 00:10:00
#SBATCH -J cms_ana_haswell
#SBATCH -L SCRATCH
#SBATCH -C haswell


module unload python
module load python/2.7-anaconda
module load h5py-parallel/2.7.1

srun -n 128 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 0 $2 $3
srun -n 128 --cpu_bind=rank python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 1 $2 $3
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 2
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 3
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 4
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 5
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 6
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 7
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 8
#srun -n 128 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron haswell na 9

#sed -i '1s/^/Nodes Cores Label Time Platform Memmode Run\n/' $1 
