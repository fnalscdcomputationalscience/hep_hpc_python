#!/bin/bash -l
#SBATCH --account=m1523
#SBATCH -p regular
#SBATCH -N 4
#SBATCH -t 02:00:00
#SBATCH -J cms_ana
#SBATCH -L SCRATCH
#SBATCH -C knl,quad,cache


module unload python
module load python/2.7-anaconda
module load h5py-parallel/2.7.1

srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 0
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 1
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 2
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 3
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 4
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 5
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 6
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 7
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 8
srun -n 272 python-mpi ~/hep_hpc_python/hist_test.py /global/cscratch1/sd/ssehrish/h5out_merged/ Electron knl cache 9

sed -i '1s/^/Nodes Cores Label Time Platform Memmode Run\n/' $1 
