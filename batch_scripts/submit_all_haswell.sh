#!/bin/bash 

cores=$1
nodes=$2
platform=$3

outfile=$4
barrier=$5

for (( c=0; c<=1; c++ ))
do  
   file="hist_${platform}_${cores}_${nodes}_${c}.o"
   sbatch --output=$file /global/homes/s/ssehrish/hep_hpc_python/batch_scripts/run-${platform}-${nodes}.sl $file ${c} /global/cscratch1/sd/ssehrish/haswell_results/$outfile $barrier
done

