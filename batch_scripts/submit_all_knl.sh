#!/bin/bash 

cores=$1
nodes=$2
platform=$3
mem=$4


for (( c=0; c<=9; c++ ))
do  
   file="hist_${platform}_${mem}_${cores}_${nodes}_${c}.o"
   sbatch --output=$file ../batch_scripts/run-${platform}-${mem}-${nodes}.sl $file
done

