from mpi4py import MPI
import csv
import glob
import h5py
import numpy as np
import os
import pandas as pd
import sys
import read_table
import tables
import filter_electrons as fe
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import create_df as cd

if __name__ == "__main__":
    s_time = MPI.Wtime()
    if len(sys.argv) < 9:
        print(
            "Usage: ",
            sys.argv[0],
            "input-dir group-name platform mem-mode run-num batch-num outpath barrier",
        )
        sys.exit(-1)
    myrank = MPI.COMM_WORLD.rank
    ranks = MPI.COMM_WORLD.size
    for x in range(0, 1):
        in_dir = sys.argv[1]
        grp_name = sys.argv[2]
        platform = sys.argv[3]
        mem_mode = sys.argv[4]
        run_num = sys.argv[5]
        batch_num = sys.argv[6]
        outpath = sys.argv[7]
        barrier = sys.argv[8]
        l = []
        if platform == "haswell":
            cores = 32
        elif platform == "knl":
            cores = 68
        else:
            print("platform should be either haswell or knl")
            sys.exit(-1)

        elec_df = cd.create_df_all(in_dir, grp_name, myrank, ranks)
        local_elec = len(elec_df.index)
        info_df = cd.create_df_all(in_dir, "Info", myrank, ranks)
        local_events = len(info_df.index)
        df = pd.merge(elec_df, info_df, how="left", on=["evtNum", "lumisec", "runNum"])
        fdf = fe.filter_electrons(df)
        local_fe = len(fdf.index)

        local_max = fdf["pt"].max()
        local_min = fdf["pt"].min()

        num_events = MPI.COMM_WORLD.reduce(local_events, op=MPI.SUM, root=0)
        num_elecs = MPI.COMM_WORLD.reduce(local_elec, op=MPI.SUM, root=0)
        num_fes = MPI.COMM_WORLD.reduce(local_fe, op=MPI.SUM, root=0)
        global_min = MPI.COMM_WORLD.reduce(local_min, op=MPI.MIN, root=0)
        global_max = MPI.COMM_WORLD.reduce(local_max, op=MPI.MAX, root=0)

        if myrank == 0:
            print(num_events, num_elecs, num_fes, global_min, global_max)
