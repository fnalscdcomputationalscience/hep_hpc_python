########################################################################
# create_lariat_batch_file.cmake
#
# Using a template, create an appropriately-named batch submission
# script based on provided parameters.
#
# Usage: cmake [<-DXXX=YYY>+] -P create_lariat_batch_file.cmake
#
# Parameters:
#
#   PHASE=#
#   1 = haswell, 2 = knl,quad,cache (default 2).
#
# * NODES=#
#   Number of nodes to reserve (default 2).
#
# * TASKS_PER_NODE=#
#   Number of tasks per node (default 136).
#
# * MEM=#
#   Per-node memory limit in GiB (default 64GiB).
#
# * TIME=##:##:##
#   Time requested for job (default 00:10:00).
#
# * QUEUE=regular|debug|premium
#  Job queue designator (default regular).
#
# * NEVT=#
#   Maximum # of events to process (default empty).
########################################################################
function(ensure_pos VAR)
  cmake_parse_arguments("EP" "" "MIN;MAX" "" ${ARGN})
  if (NOT ${VAR} GREATER 0)
    message(FATAL_ERROR "${VAR} is not a positive number (${${VAR}})")
  endif()
  if (DEFINED EP_MIN)
    if (${VAR} LESS ${EP_MIN})
      message(FATAL_ERROR "${VAR} is less than allowed minimum ${EP_MIN}")
    endif()
  endif()
  if (DEFINED EP_MAX)
    if (${VAR} GREATER ${EP_MAX})
      message(FATAL_ERROR "${VAR} exceeds allowed maximum ${EP_MAX}")
    endif()
  endif()
endfunction()

function(pad_with_zeros NUM OUTPUT_VAR DIGITS)
  execute_process(COMMAND printf "%0${DIGITS}d" ${NUM}
    OUTPUT_VARIABLE TMP
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  set(${OUTPUT_VAR} ${TMP} PARENT_SCOPE)
endfunction()

set(PHASE 2 CACHE STRING "Cori phase (1 = haswell, 2 = knl,quad,cache")
ensure_pos(PHASE MIN 1 MAX 2)
set(NODES 2 CACHE STRING "Number of nodes to request")
ensure_pos(NODES)
set(TASKS_PER_NODE 136 CACHE STRING "Number of MPI tasks per node")
ensure_pos(TASKS_PER_NODE)
set(MEM 64 CACHE STRING "Maximum per-node memory to be used in data processing (GiB)")
ensure_pos(MEM)
set(TIME "00:10:00" CACHE STRING "Time to request from batch system")
if (NOT TIME MATCHES "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9]\$")
  message(FATAL_ERROR "TIME (${TIME}) does not appear to be a valid time request")
endif()
set(QUEUE "regular" CACHE STRING "Queue designator")
if (NOT QUEUE MATCHES "^(regular|debug|premium)\$")
  message(FATAL_ERROR "QUEUE (${QUEUE}) does not appear to be a valid queue designator")
endif()

pad_with_zeros(${TASKS_PER_NODE} TPN_TXT 3)
pad_with_zeros(${NODES} NODES_TXT 5)

if (NEVT)
  set(NEVT_ARG "-n ${NEVT}")
  pad_with_zeros(${NEVT} NEVT_TXT 8)
  set(NEVT_TXT "-${NEVT_TXT}")
endif()

if (PHASE EQUAL 1)
  set(C_ARG "haswell")
else()
  set(C_ARG "knl,quad,cache")
endif()

string(SUBSTRING "${QUEUE}" 0 1 Q)
set(OUTPUT_FILE "run-LArIAT-${TPN_TXT}-${NODES_TXT}${NEVT_TXT}-${Q}-p${PHASE}.sl")
configure_file("run-LArIAT.sl.in" "${OUTPUT_FILE}" @ONLY)
message("${OUTPUT_FILE}")

