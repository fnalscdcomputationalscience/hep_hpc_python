import numpy as np
import copy


def prod_upto(args):
    b = copy.deepcopy(args)
    b.insert(0, 1)
    return list(np.cumprod(b))[:-1]


def prod_from(args):
    a = copy.deepcopy(args)
    a.reverse()
    p = list(np.cumprod(a))[:-1]
    p.reverse()
    p.append(1)
    return p


def make_indices(counts):
    tiles = prod_upto(counts)
    repeats = prod_from(counts)
    return [np.tile(np.repeat(np.arange(c), r), t) for c, t, r in zip(counts, tiles, repeats)]
