import numpy as np
from key_list import KEY_LIST

def hitsperplaneCut(tables):
    df = tables["sel_nuecosrej"]
    return df.hitsperplane < 8

def vtxelasticCut(tables):
    df = tables["vtx"]
    return df.nelastic > 0

# This does not return an answer for every slice.
def vtxelasticzCut(tables):
    df = tables["vtx_elastic"]
    df['good'] = (df.vtxid == 0) & (df.npng3d > 0)
    return df.groupby(KEY_LIST)["good"].agg(np.any)

def showersperplaneCut(tables):
    df = tables["shw"]
    return df.nshwlid > 0

# toy implementation to check how to use multiple rows
# needs to be re implemented using the dot product
def leading_showers_angle_cut(grp):
    if len(grp) == 1:
        return True
    grp.dirX[0] == grp.dirX[1]

def shwlidCut(tables):
    df = tables["shw_shwlid"]
    return (df.showerid == 0) & (df.nhitx > 5) & (df.nhity > 5) & (df.gap <= 100)

def filter(tables):
    return hitsperplaneCut(tables) & vtxelasticCut(tables) & showersperplaneCut(tables) & shwlidCut(tables)

if __name__ == "__main__":
    pass
