def kNueSecondAnaPtPCut(tables):
    nuecosrej = tables["sel_nuecosrej"]
    shw = tables["shw"]
    shwlid = tables["shw_shwlid"]

    shw['maxY'] = shwlid.groupby(['run', 'subRun', 'event', 'slice'])['startY', 'stopY'].max().max(axis=1)
    shw['ptp'] = nuecosrej.partptp 

    return shw.maxY > 600 & shw.ptp < 0.4 | shw.maxY < 600 & shw.ptp 
if __name__ == "__main__": 
    pass
