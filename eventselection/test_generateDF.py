import numpy as np

from eventselection.generateDF import make_indices
from eventselection.generateDF import prod_from
from eventselection.generateDF import prod_upto

import unittest

class Test_generateDF(unittest.TestCase):
    def setUp(self):
        self.args = [2, 2, 3, 3, 2]

    def test_prod_upto(self):
        result = prod_upto(self.args)
        assert (result == [1, 2, 4, 12, 36])

    def test_prod_from(self):
        result = prod_from(self.args)
        assert (result == [36, 18, 6, 2, 1])

    def test_make_indices_4(self):
        # Do not use the "vertices"
        results = make_indices(self.args[:-1])
        assert (len(results) == 4)
        r, sr, evts, slc = results
        assert (len(r) == 36)
        assert (len(sr) == 36)
        assert (len(evts) == 36)
        assert (len(slc) == 36)
        assert (np.all(r[:18] == 0))
        assert (np.all(r[18:] == 1))
        assert (np.all(sr[:9] == 0))
        assert (np.all(sr[9:18] == 1))
        assert (np.all(evts[:3] == 0))
        assert (np.all(evts[3:6] == 1))
        assert (np.all(evts[6:9] == 2))
        assert (np.array_equal(slc, np.resize([0, 1, 2], 36)))

    def test_make_indices_5(self):
        results = make_indices(self.args)
        assert (len(results) == 5)
        r, sr, evts, slc, vtx = results
        assert (len(r) == 72)
        assert (len(sr) == 72)
        assert (len(evts) == 72)
        assert (len(slc) == 72)
        assert (len(vtx) == 72)
        assert (np.all(r[:36] == 0))
        assert (np.all(r[36:] == 1))
        assert (np.all(sr[:18] == 0))
        assert (np.all(sr[18:36] == 1))
        assert (np.all(evts[:6] == 0))
        assert (np.all(evts[6:12] == 1))
        assert (np.all(evts[12:18] == 2))
        assert (np.array_equal(slc, np.resize([0, 0, 1, 1, 2, 2], 72)))
        assert (np.array_equal(vtx, np.resize([0, 1], 72)))
