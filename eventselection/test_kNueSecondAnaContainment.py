from __future__ import print_function
import pandas as pd
import numpy as np
from numpy import clip
from numpy.random import random
import generateDF

import unittest

import kNueSecondAnaContainment as knsa

DATA_COLUMN_NAMES = ["distallpngtop", "distallpngbottom",
                     "distallpngeast", "distallpngwest",
                     "distallpngfront", "distallpngback"]

class Test_kNueAnaSecondAnaContainment(unittest.TestCase):
    def setUp(self):
        self.tables =  {}
        r, sr, ev, slc = generateDF.make_indices([2,3,5,7])
        self.numslices = len(r)
        df = pd.DataFrame({"run": r,
                          "subRun": sr,
                          "event": ev,
                          "slice": slc,
                          "distallpngtop": clip(random(self.numslices)*100, 0., 63.),
                          "distallpngbottom": clip(random(self.numslices)*20, 0., 12.),
                          "distallpngeast": clip(random(self.numslices)*20, 0., 12.),
                          "distallpngwest": clip(random(self.numslices)*20, 0., 12.),
                          "distallpngfront": clip(random(self.numslices)*30, 0., 18.),
                          "distallpngback": clip(random(self.numslices)*30, 0., 18.)
                          })
        self.tables["sel_nuecosrej"] = df

    def test_all_bad(self):
        df = self.tables["sel_nuecosrej"]
        assert isinstance(df, pd.DataFrame)
        df.loc[10, DATA_COLUMN_NAMES] = [63.0, 12.1, 12.1, 12.1, 18.1, 18.1]
        # Make sure replacing the slice of data worked as we expected.
        assert all(df.loc[10, DATA_COLUMN_NAMES] == [63.0, 12.1, 12.1, 12.1, 18.1, 18.1])
        df.loc[11, DATA_COLUMN_NAMES] = [63.1, 12.0, 12.1, 12.1, 18.1, 18.1]
        df.loc[12, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.0, 12.1, 18.1, 18.1]
        df.loc[13, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.0, 18.1, 18.1]
        df.loc[14, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.1, 18.0, 18.1]
        df.loc[15, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.1, 18.1, 18.0]

        res = knsa.kNueSecondAnaContainment(self.tables)
        assert (len(res) == self.numslices)
        assert (np.all(res == False))

    def test_one_good(self):
        df = self.tables["sel_nuecosrej"]
        df.loc[13, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.1, 18.1, 18.1]
        assert all(df.loc[13, DATA_COLUMN_NAMES] == [63.1, 12.1, 12.1, 12.1, 18.1, 18.1])
        res = knsa.kNueSecondAnaContainment(self.tables)
        assert (len(res) == self.numslices)
        assert isinstance(res, pd.core.series.Series)
        assert (res[13])
        assert (np.sum(res) == 1) # only 1 True in the whole array.

    def test_data_with_nans(self):
        df = self.tables["sel_nuecosrej"]

        df.loc[10, DATA_COLUMN_NAMES] = [np.NaN, 12.1, 12.1, 12.1, 18.1, 18.1]
        df.loc[11, DATA_COLUMN_NAMES] = [63.1, np.NaN, 12.1, 12.1, 18.1, 18.1]
        df.loc[12, DATA_COLUMN_NAMES] = [63.1, 12.1, np.NaN, 12.1, 18.1, 18.1]
        df.loc[13, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, np.NaN, 18.1, 18.1]
        df.loc[14, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.1, np.NaN, 18.1]
        df.loc[15, DATA_COLUMN_NAMES] = [63.1, 12.1, 12.1, 12.1, 18.1, np.NaN]

        res = knsa.kNueSecondAnaContainment(self.tables)
        assert (len(res) == self.numslices)
        assert (np.all(res == False))
