import pandas as pd
import numpy as np

import kNueSecondAnaDQ as knsa

from generateDF import make_indices
from functools import reduce
from operator import mul

class Test_kNueSecondAnaDQ:
    def setUp(self):
        self.tables = {}
        counts = [2, 3, 5, 3, 7]
        num_slices = reduce(mul, counts[:-1]) #nr * nsr * ne * nsl
        # Table 1 has one row per slice
        hpp = np.ones(num_slices)
        hpp *= 10 # most values are big and will fail our cut
        hpp[15] = 0
        hpp[3] = 7
        hpp[28] = 8
        self.tables["sel_nuecosrej"] = \
             pd.DataFrame({"hitsperplane": hpp,
                       "slice": np.tile(0, num_slices)})
        # Table 2 has one row per vertex
        run, subRun, event, slc, vtxid = make_indices(counts)
        num_total = reduce(mul, counts)
        npng3d = np.zeros(num_total, dtype = np.int)
        assert(len(npng3d) == num_total)

        df = pd.DataFrame({"run": run,
                            "subRun": subRun,
                            "event": event,
                            "slice": slc,
                            "vtxid": vtxid,
                            "npng3d": npng3d})
        # Here I want to set some npng3d values to non-zero, based on
        # run/subRun/event/slice/vtxid.
        #   1/2/3/1/0 = 3
        #   1/2/4/2/3 = 5
        # This will make slice 1/2/3/1 pass,
        # and all others should fail.
        # How do we set the data in the DataFrame?

        self.tables["vtx_elastic"] = df

    def tearDown(self):
        pass


    def test_hitsperplaneCut(self):
        results = knsa.hitsperplaneCut(self.tables)
        assert(np.sum(results) == 2)
