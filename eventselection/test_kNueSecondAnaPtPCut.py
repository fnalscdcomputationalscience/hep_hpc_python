import pandas as pd
import numpy as np
from numpy import clip
from numpy.random import random
import generateDF

import unittest

import kNueSecondAnaPtPCut as kpt

class Test_kNueAnaSecondAnaPtPCut(unittest.TestCase):
    def setUp(self):
        self.tables =  {}
        indices = [2, 3, 5, 7, 3]
        r, sr, ev, slc = generateDF.make_indices(indices[:-1])
        self.numslices = len(r)
        df = pd.DataFrame({"run": r,
                           "subRun": sr,
                           "event": ev,
                           "slice": slc,
                           "partptp": clip(random(self.numslices)*0.65, 0., 0.65)
                         })

        r, sr, ev, slc, shw = generateDF.make_indices(indices)
        self.numshowers = len(r)
        shwlid_df = pd.DataFrame({"run": r,
                          "subRun": sr,
                          "event": ev,
                          "slice": slc,
                          "shower": shw,
                          "startX": clip(random(self.numshowers)*300, 0., 600.),
                          "startY": clip(random(self.numshowers)*300, 0., 600.),
                          })

        self.tables["sel_nuecosrej"] = df
        self.tables["shw_shwlid"] = shwlid_df

    def test_all_bad(self):
        assert False

    def test_one_good(self):
        assert False
