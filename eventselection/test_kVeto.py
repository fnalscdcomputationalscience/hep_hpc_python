from __future__ import print_function
import pandas as pd
import numpy as np
from numpy import clip
from numpy.random import random
import generateDF

import unittest

import kVeto as kv

class Test_kVeto(unittest.TestCase):
    def setUp(self):
        self.tables =  {}
        r, sr, ev, slc = generateDF.make_indices([2,3,5,7])
        self.numslices = len(r)
        df = pd.DataFrame({"run": r,
                          "subRun": sr,
                          "event": ev,
                          "slice": slc,
                          "keep": np.zeros(self.numslices, dtype = np.bool)
                          })
        self.tables["sel_veto"] = df

    def test_all_bad(self):
        df = self.tables["sel_veto"]

        res = kv.kVeto(self.tables)
        assert (len(res) == self.numslices)
        assert (np.all(res == False))

    def test_one_good(self):
        df = self.tables["sel_veto"]
        df.loc[13, "keep"] = True
        res = kv.kVeto(self.tables)
        assert (len(res) == self.numslices)
        assert (res[13])
        assert (np.sum(res) == 1) # only 1 True in the whole array.
