from mpi4py import MPI
import glob
import h5py
import numpy as np
import os
import pandas as pd
import sys
import read_table
import tables
import filter_electrons as fe
import create_df as cdf

# import line_profiler
# from memory_profiler import profile
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import math


def start_end(ds_name, grp, myrank, ranks):
    """ given a list of group names to be used as key, 
    size of dataset to be read, return a dataframe with 
    process rank and what partition do they need to read.  
    """
    d = grp[ds_name]
    var_size = d.size
    start = var_size / ranks * myrank
    end = start + int(math.ceil(float(var_size) / ranks))
    if (var_size % ranks) != 0:
        end = end - 1
    if myrank == ranks - 1:
        end = var_size
    return start, end


def update_end(df, overlap):
    local_end = len(df.index)
    local_start = local_end - overlap
    mydiff = df[local_start:local_end].diff()
    myloc = mydiff.loc[~(mydiff == 0).all(axis=1)]
    return myloc[myloc.index > local_start].head(1).index.values[0]


def update_start(df, overlap):
    mydiff = df[0:overlap].diff()
    myloc = mydiff.loc[~(mydiff == 0).all(axis=1)]
    return myloc[myloc.index > 0].head(1).index.values[0]


# this will be replace the existing read_ds
def read_dataset(ds_name, grp, start, end):
    d = grp[ds_name]
    return d[start:end]


# this will replace the existing read_table
def create_table(columns, grp, start, end):
    return {colname: read_dataset(colname, grp, start, end) for colname in columns}


def create_pdf(columns, grp, start, end):
    return pd.DataFrame(create_table(columns, grp, start, end))


def adjusted_start_end(grp, columns, myrank, ranks, overlap):
    # use any dataset name within the group grp, all datasets have
    # equal length
    start, end = start_end(columns[0], grp, myrank, ranks)
    if myrank != ranks - 1:
        end = overlap + end

    df = create_pdf(columns, grp, start, end)

    # update my local start, for all except the first rank
    if myrank != 0:
        mystart = update_start(df, overlap)
    else:
        mystart = 0

    # update my local end, for all except the last rank
    if myrank != ranks - 1:
        myend = update_end(df, overlap)
    else:
        myend = len(df.index)

    return mystart + start, mystart + start + (myend - mystart)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: ", sys.argv[0], "input-dir group-name")
        sys.exit(-1)
    s_time = MPI.Wtime()
    fname = sys.argv[1]
    grp_name = sys.argv[2]
    myrank = MPI.COMM_WORLD.rank
    ranks = MPI.COMM_WORLD.size
    columns = ["Electron.evtNum", "Electron.runNum", "Electron.lumisec"]
    overlap = 500
    # we assume only one large file here
    f = h5py.File(fname, "r", driver="mpio", comm=MPI.COMM_WORLD)
    grp = f["/" + grp_name]
    d = grp[columns[0]]
    var_size = d.size

    start, end = adjusted_start_end(grp, columns, myrank, ranks, overlap)

    # here create electron dataframe and call gorup_by to see
    # some CMS specific code, to remove the metadata columns not needed
    ds_names = grp.keys()
    ds_names.remove("snum")
    ds_names.remove("stype")

    # this may change once we decide whether we have to give one large
    # file name or dir path for several small files
    in_dir = os.path.dirname(fname)
    # we will be using either one of the following creat functions
    # after we agree on the interface
    info_df = cdf.create_df_all(in_dir, "Info", myrank, ranks)
    df = create_pdf(ds_names, grp, start, end)
    # CMS specific stuff
    old_cols = df.columns
    df.columns = [cdf.rename_column(e, grp_name) for e in old_cols]
    fdf = pd.merge(df, info_df, how="left", on=["evtNum", "lumisec", "runNum"])
    fdf = fe.filter_electrons(fdf)
    mynum = len(fdf.index)
    # global reduce
    num = MPI.COMM_WORLD.reduce(mynum, op=MPI.SUM, root=0)
    if myrank == 0:
        print(num)
