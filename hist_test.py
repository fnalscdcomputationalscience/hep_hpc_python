from mpi4py import MPI
import csv
import glob
import h5py
import numpy as np
import os
import pandas as pd
import sys
import read_table
import tables
import filter_electrons as fe
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import create_df as cd

if __name__ == "__main__":
  s_time = MPI.Wtime()
  if len(sys.argv) < 9:
    print("Usage: ", sys.argv[0], "input-dir group-name platform mem-mode run-num batch-num outpath barrier")
    sys.exit(-1)
  myrank = MPI.COMM_WORLD.rank  
  ranks = MPI.COMM_WORLD.size
  for x in range(0, 1):
    in_dir = sys.argv[1]
    grp_name = sys.argv[2]
    platform = sys.argv[3]
    mem_mode = sys.argv[4]
    run_num = sys.argv[5]
    batch_num = sys.argv[6]
    outpath = sys.argv[7]
    barrier = sys.argv[8]
    l = []
    if platform == 'haswell': 
      cores = 32
    elif platform == 'knl':
      cores = 68
    else: 
      print("platform should be either haswell or knl") 
      sys.exit(-1)  
    l.append([myrank, ranks/cores, ranks, "Setup", MPI.Wtime(), MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num])
 
    if barrier == 1 or barrier == 2: 
      MPI.COMM_WORLD.Barrier()
    s_time = MPI.Wtime()
    elec_df = cd.create_df_all(in_dir, grp_name, myrank, ranks)
    info_df = cd.create_df_all(in_dir, "Info", myrank, ranks)
    l.append([myrank, ranks/cores, ranks, "Read", MPI.Wtime(), MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num]) 
    if barrier == 2: 
      MPI.COMM_WORLD.Barrier()

    s_time = MPI.Wtime()
    df = pd.merge(elec_df, info_df, how='left', on=['evtNum', 'lumisec', 'runNum'])
    l.append([myrank, ranks/cores, ranks, "Merge", MPI.Wtime(), MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num])

    s_time = MPI.Wtime()
    fdf = fe.filter_electrons(df)
    l.append([myrank, ranks/cores, ranks, "Filter", MPI.Wtime(), MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num])

    s_time = MPI.Wtime()
    localhistpt, bin_edges = np.histogram(fdf['pt'], bins=[0, 20, 30, 40, 50, 100, 500]) 
    l.append([myrank, ranks/cores, ranks, "Myplot", MPI.Wtime(), MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num])

    s_time = MPI.Wtime()
    globalhist = MPI.COMM_WORLD.reduce(localhistpt, op=MPI.SUM, root=0)
    l.append([myrank, ranks/cores, ranks, "Plot", MPI.Wtime() ,MPI.Wtime() - s_time, platform, mem_mode, run_num, batch_num])

    with open(outpath+"_%s.csv" %myrank, "a") as f:
      writer = csv.writer(f)
      writer.writerows(l)

    if myrank==0: 
      print globalhist
#        plt.hist(globalhist)
#        plt.savefig("myplot%s"%ranks)
