
#' load_lariat_timing
#'
#' @param fname : name of tab separated values file to read
#'
#' @return a tibble containing all the data, but with LocalTime replaced by "t", which starts at 0.
#' @export
#'

load_lariat_timing <- function(fname, ranks_per_node)
{
  x <- readr::read_tsv(fname)
  x %>% mutate(t = LocalTime - min(LocalTime),
               node = Rank %/% ranks_per_node) %>%
        select(-"LocalTime")
}

#' make_iter_dataframe
#'
#' @param fulldf : full data set tibble, as returned by load_lariat_timing
#'
#' @return a tibble with timing information for each iteration.
#' @export
#'
make_iter_dataframe <- function(fulldf)
{
  # Keep only the iteration start and stop markers.
  x <- fulldf %>%
    filter(Stage %in% c("IterationStart",
                        "IterationEnd"))
  # Spread the data. We have to remove the Nevt column to do so, because they
  # carry the "wrong value" (the -1 missing value marker).
  y <- x %>%
    select(-"Nevt") %>%
    spread(Stage, t)
  # Now get the right value of Nevt to staple back in...
  z <- x %>%
    filter(Nevt > 0) %>%
    select(Nevt)
  y$Nevt = z$Nevt
  # Calculate the running time, and put back the Rank and Iter columns.
  y %>% mutate(titer = IterationEnd - IterationStart)
}

#' make_read_dataframe
#'
#' @param fulldf : full data set tibble, as returned by load_lariat_timing
#'
#' @return a tibble with timing for the reading of data for each iteration
#' @export
#'
make_read_dataframe <- function(fulldf)
{
  # Keep only the data reading start and stop markers
  x <- fulldf %>%
    filter(Stage %in% c("BeforeDataRead", "AfterDataRead"))
  # Spread the data. We have to remove the Nevt column to do so, because they
  # carry the "wrong value" (the -1 missing value marker).
  y <- x %>%
    select(-"Nevt") %>%
    spread(Stage, t)
  # Calculate reading time.
  y %>%
    mutate(tread = AfterDataRead - BeforeDataRead)
}

combine_iter_dataframes <- function(diter, dread)
{
  x <- bind_cols(diter,
                 dread %>% select(-"Rank", -"Iter", -"node"))
  x %>% mutate(tfft = titer - tread)
}

make_one_job_dataframe <- function(fname, ranks_per_node)
{
  tmp <- load_lariat_timing(fname, ranks_per_node)
  d2 <- make_iter_dataframe(tmp)
  d3 <- make_read_dataframe(tmp)
  res <- combine_iter_dataframes(d2, d3)
  res$rpn <- ranks_per_node
  res$nodes <- max(res$Rank + 1) %/% ranks_per_node
  res
}

#' make_combined_dataframe
#'
#' @return a dataframe containing all iteration measurements from the named files.
#' @export
#'
make_combined_dataframe <- function()
{
  t1 <- make_one_job_dataframe("datafiles/timing-064-00200.dat.bz2", ranks_per_node = 64)
  t2 <- make_one_job_dataframe("datafiles/timing-064-01200.dat.bz2", ranks_per_node = 64)
  t3 <- make_one_job_dataframe("datafiles/timing-136-00200.dat.bz2", ranks_per_node = 136)
  t4 <- make_one_job_dataframe("datafiles/timing-136-00500.dat.bz2", ranks_per_node = 136)
  t5 <- make_one_job_dataframe("datafiles/timing-136-01200.dat.bz2", ranks_per_node = 136)
  t6 <- make_one_job_dataframe("datafiles/timing-068-00200.dat.bz2", ranks_per_node = 68)
  t7 <- bind_rows(t1, t2, t3, t4, t5, t6)
  t7$plat = "knl"
  t8 <- make_one_job_dataframe("datafiles/timing-032-00200-p1.dat.bz2", ranks_per_node = 32)
  t8$plat = "haswell"
  bind_rows(t7, t8)
}

make_summary_dataframe <- function(d)
{
  q <- d %>%
    group_by(rpn, nodes, plat) %>%
    summarize(niter = max(Iter)+1,
              nranks = max(Rank)+1,
              nevt = median(Nevt),
              begin = min(IterationStart),
              end = max(IterationEnd))
  q %<>% mutate(tot = (end - begin))
  normalization <- q[1,]$tot
  q %>% mutate(totnorm = tot/normalization)
}
