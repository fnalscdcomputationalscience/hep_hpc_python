#!/bin/bash
#COBALT -t 30
#COBALT -q debug
#COBALT -n 2
#COBALT -A SDL_Workshop

# Use the recommended Python environment, with all
# the packages we need.
module unload xalt  # unload this for python to work
module load alcfpython

# make of record of what module are loaded (for debugging)
module list

#aprun -n 8 -N 4 singularity run -B /opt:/opt:ro -B /var/opt:/var/opt:ro myapp.img
date
aprun -B mpirun -np 128 python process_lariat.py /projects/EnergyFEC_3/cms/datasets/stripe_48_4000M/lariat.hdf5
date

