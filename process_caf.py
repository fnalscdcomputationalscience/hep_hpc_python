class Error(Exception):
    def __init__(self, msg):
        super(Error, self).__init__(msg)


try:
    import mpi4py
    from mpi4py import MPI
except:
    raise Error("mpi4py not found; maybe set up the required UPS products?")

from h5py import File
import numpy as np
import sys
from mpi_numerology import calculate_slice_for_rank
import pandas as pd
from eventselection.key_list import KEY_LIST
from eventselection import kNueSecondAnaDQ

# Number of bytes in a GiB
GIGABYTE = 1024 ** 3
EXTRA_SPACE = 30
# KEY_LIST = ['run','subRun','event','slice']
KEYS = set(KEY_LIST)

debug_out = None


def print_one(r, msg):
    """Print the given string from only one rank (rank 0)."""
    if r == 0:
        print(msg)


def id_first_diff(run, subrun, event, slice):
    # argwhere gives back len(answer)==0 when no match
    # argwhere givew back array of arrays, where first array[0] is
    # index of first interestint one
    drun = np.diff(run) != 0
    dsubrun = np.diff(subrun) != 0
    devent = np.diff(event) != 0
    dslice = np.diff(slice) != 0

    ans = drun | dsubrun | devent | dslice
    # ans = np.logical_or(np.logical_or(np.logical_or(drun,dsubrun),devent),dslice)
    pos = np.argwhere(ans == True)
    if len(pos) == 0:
        return 0
    else:
        return pos[0][0] + 1


def get_bounds_subrun(group, rank, n_ranks):
    r = group["run"]
    s = group["subRun"]
    l = r.shape[0]

    slice_start, slice_stop = calculate_slice_for_rank(rank, n_ranks, l)
    return (slice_start, slice_stop)


def table_needs_balance(gname):
    return True
    top_level = True

    if gname.count("_") > 0:
        top_level = False

    if gname[0:4] == "sel_":
        top_level = False

    return not top_level


def get_bounds_slice(group, gname, rank, n_ranks):

    r = group["run"]
    s = group["subRun"]
    e = group["event"]
    c = group["slice"]
    l = len(r)

    if l == 0:
        # print("get bounds:", gname, "has no run records")
        return (-1, -1)

    slice_start, slice_stop = calculate_slice_for_rank(rank, n_ranks, l)

    if table_needs_balance(gname) == False:
        return (slice_start, slice_stop)

    if rank != 0 and slice_start > 0:
        read_start = slice_start - 1
    else:
        read_start = slice_start

    if rank != (n_ranks - 1):
        read_stop = slice_stop + EXTRA_SPACE
    else:
        read_stop = slice_stop

    # need the beginning from slice_start to that + EXTRA_SPACE
    s_r = r[read_start:EXTRA_SPACE]
    s_s = s[read_start:EXTRA_SPACE]
    s_e = e[read_start:EXTRA_SPACE]
    s_c = c[read_start:EXTRA_SPACE]

    # need slice_end-EXTRA_SPACE to slice_end
    e_r = r[read_stop - EXTRA_SPACE - 1 : slice_stop]
    e_s = s[read_stop - EXTRA_SPACE - 1 : slice_stop]
    e_e = e[read_stop - EXTRA_SPACE - 1 : slice_stop]
    e_c = c[read_stop - EXTRA_SPACE - 1 : slice_stop]

    s = id_first_diff(s_r, s_s, s_e, s_c)
    e = id_first_diff(e_r, e_s, e_e, e_c)

    debug_out.write("%d length %d %d %d\n" % (rank, len(e_r), len(e_s), len(e_e)))
    debug_out.write("%d adjustment %d %d\n" % (rank, s, e))
    debug_out.write("%d start/stop %d %d\n" % (rank, slice_start, slice_stop))

    # this does not account for max rank - 1 running over the end with the addition of EXTRA_SPACE
    if rank != 0:
        slice_start = slice_start + s
    if rank != (n_ranks - 1):
        slice_stop = slice_stop + e

    return (slice_start, slice_stop)


def print_junk(rank, all_tables):
    fout = open("rank" + str(rank) + ".txt", "w")
    for a in list(all_tables.items()):
        fout.write("----" + str(a[0]) + "\n")
        df = pd.DataFrame(a[1])
        for b in list(a[1].items()):
            fout.write(str(b[0]) + str(b[1].shape) + "\n")


def do_cuts(tables):
    all = []
    temp = {}
    temp["trk_cosmic"] = tables["trk_cosmic"]
    for k, df in list(temp.items()):  # was tables
        # df=pd.DataFrame(v)
        ecut = df.calE > 3.0
        dirxcut = df.dirX < 0.0
        df["egood"] = ecut
        df["dgood"] = dirxcut
        b = df.groupby(KEY_LIST, as_index=False)["egood", "dgood"].agg(np.any)
        all.append(b)
    return all
    # use lexsort on run, subrun, event, slice


def generator_index(rank, nranks):
    pass


def get_subruns_index(rank, nranks, group, gname):
    """get a subrun index for table defined by group

    the returned object is a n,6 matrix, the columns are:
    * rank
    * file_offset_start
    * file_offset_end
    * run number
    * subRun number
    * event number

    """
    comm_world = MPI.COMM_WORLD
    num_columns = 6

    if rank == n_ranks - 1:
        extra = 0
    else:
        extra = 1

    slice_start, slice_stop = get_bounds_slice(group, gname, rank, n_ranks)
    if slice_start == -1:
        # print("get index: no slice bounds:",rank, gname, slice_start, slice_stop)
        return pd.DataFrame()

    r_df = group["run"]
    s_df = group["subRun"]
    e_df = group["event"]
    slice_r = r_df[slice_start : (slice_stop + extra)]
    slice_s = s_df[slice_start : (slice_stop + extra)]
    slice_e = e_df[slice_start : (slice_stop + extra)]
    slice_r.shape = (slice_r.shape[0],)
    slice_s.shape = (slice_s.shape[0],)
    slice_e.shape = (slice_e.shape[0],)

    if len(slice_r) == 0:
        last_one = 0
        last_run = 0
        last_subrun = 0
        last_event = 0
    else:
        last_one = -1
        last_run = slice_r[last_one]
        last_subrun = slice_s[last_one]
        last_event = slice_e[last_one]

    debug_out.write(
        "%d, %s start/stop %d %d\n" % (rank, gname, slice_start, slice_stop)
    )
    debug_out.write(
        "%d %s last %d, %d, %d\n" % (rank, gname, last_run, last_subrun, last_event)
    )

    rs_df = pd.DataFrame(
        data={
            "run": slice_r[0:last_one],
            "subRun": slice_s[0:last_one],
            "event": slice_e[0:last_one],
        }
    )

    # calculate offsets using multiindex
    # calling unique here might be better
    rs_index = pd.MultiIndex.from_arrays(
        [rs_df["run"], rs_df["subRun"], rs_df["event"]]
    )
    rs_df.set_index(rs_index, inplace=True)
    rs_df.drop_duplicates(keep="first", inplace=True)
    entries = rs_df.index

    starts = [slice_start + rs_index.get_loc(x).start for x in entries]
    stops = [slice_start + rs_index.get_loc(x).stop for x in entries]
    ranks = [rank] * len(starts)  # inx.shape[0]

    # does not work:
    # print( rank, rs_df.get( last_run, last_subrun, last_event ) )

    if (
        rank != (n_ranks - 1)
        and entries.contains((last_run, last_subrun, last_event)) == True
    ):
        ranks[-1] = rank + 1

    #    if rank==n_ranks-1 or (last_run,last_subrun,last_event) in entries == False:
    #        pass
    #    else:
    #        ranks[-1]=rank+1

    inx = rs_df.values.copy()
    df = np.transpose(np.array([ranks, starts, stops], dtype=inx.dtype))
    all_data = np.concatenate((df, inx), axis=1)

    debug_out.write("%s\n" % (str(all_data)))

    # exchange counts
    all_cnts = np.empty(nranks, dtype=np.int32)
    mycnt = np.full((1,), len(all_data), dtype=np.int32)
    comm_world.Allgather(mycnt, all_cnts)
    all_cnts = all_cnts * num_columns
    all_offsets = np.roll(all_cnts, 1).cumsum() - all_cnts[-1]

    all_ret = np.zeros(all_cnts.sum(), dtype=np.int32)
    comm_world.Allgatherv(all_data, [all_ret, all_cnts, all_offsets, MPI.INTEGER])
    all_ret.shape = (len(all_ret) // num_columns, num_columns)

    # before the return, we need to merge duplicates because events can cross node
    # boundaries i.e. slices cannot be cut, but our ownership of blocks is by
    # (run, subRun, eventxs)

    ind = pd.DataFrame(
        all_ret,
        # index=['run','subrun','event'],
        columns=["rank", "start", "end", "event", "run", "subrun"],
    )
    ind.set_index(["run", "subrun", "event"], inplace=True)
    # debug_out.write("%d, %s\n"%(rank, gname))
    # debug_out.write("%s\n"%(str(ind)))

    return ind


def print_results(res):
    for x in res.group:
        print("-----")
        print(x)


def use_global_index(g, gname, rank, n_ranks, global_index):
    # note: this will get the index for this group

    idx = get_subruns_index(rank, n_ranks, g, gname)

    # convert to dataframes (this should be done in the called routine)

    # (1) select records from my rank from global index
    #   (comes in this way)

    # do intersection of that selection with idx
    # global_index.join(idx) # , how='inner')
    if len(idx) > 0:
        new_one = idx.join(global_index, how="inner", rsuffix="j")
        debug_out.write("%d %s\n" % (rank, str(new_one)))
        debug_out.write(
            (
                "range %d %s %d %d\n"
                % (rank, gname, np.min(new_one.start), np.max(new_one.end))
            )
        )
        # if gname == 'vtx':
        #    f=file("info"+str(rank)+'.txt', 'w')
        #    f.write(str(new_one)+'\n')
        #    f.write(str(global_index)+'\n')
        return (np.min(new_one.start), np.max(new_one.end))
        # return (new_one.start[0], new_one.end[-1])
    else:
        # calculate full range (lower start and highest stop values)
        # return get_bounds_slice(g,gname,rank,n_ranks)
        return (-1, -1)


def process_group(g, gname, rank, n_ranks, global_index):
    """Create a dataframe for the given table name (group)

    Important item: if this is an independent table, then just use the
    slice bounds as the slice location procedure.   If the table
    participates in composite questions, then it needs to use the global index
    to determine the slice start/stop values.
    """

    if table_needs_balance(gname):
        slice_start, slice_stop = use_global_index(
            g, gname, rank, n_ranks, global_index
        )
    else:
        slice_start, slice_stop = get_bounds_slice(g, gname, rank, n_ranks)

    if slice_start == -1:
        # print("no run information for",gname)
        return (-1, -1, {})

    dsnames = set(g.keys())  # .difference(KEYS)
    all_ds = dict()
    for dname in dsnames:
        ds = g[dname]
        if ds.shape[1] == 0:
            continue
        data = ds[slice_start:slice_stop]
        if data.shape[1] > 1:
            # print("yuck for array length",dname, gname, data.shape)
            for i in range(0, data.shape[1]):
                dtemp = data[:, i]
                dtemp.shape = (data.shape[0],)
                all_ds[dname + "|" + str(i)] = dtemp
        else:
            data.shape = (data.shape[0],)
            all_ds[dname] = data

    return (slice_start, slice_stop, all_ds)


# @profile
def process_file(infile, rank, n_ranks, global_index):
    """Process the given HDF5 File object with LArIAT-like waveform processing.

    :param infile: an open HDF5 File object with LArIAT waveform data.
    :param rank: the rank of the current process
    :param n_ranks: the total number of ranks in the process
    :return: None

        # very painful to adjust the bounds here according to slice change here
        # need a different strategy - get run,slice,event,subrun out of group first
        # could save the bounds too.  This is probably good to do.
    """

    all_tables = dict()
    gnames = list(infile.keys())
    # gnames = ['vtx'] # JBK - temporary

    for gname in gnames:
        slice_start, slice_stop, all_ds = process_group(
            infile[gname], gname, rank, n_ranks, global_index
        )
        all_tables[gname] = pd.DataFrame(all_ds)

    return all_tables


def test_build_results_table(rank, who_owns_what, header, num_cuts):
    grouped = header.groupby(["run", "subRun"]).count()

    new_head = header.set_index(["run", "subRun"])
    # new_head.sort_index(inplace=True)

    total_rows = grouped["event"].sum()
    bool_arr = np.full((total_rows, num_cuts), False, dtype=np.bool)
    barr = np.full((total_rows,), False, dtype=np.bool)
    df = pd.Series(barr, index=new_head.index)
    # print(new_head.head())
    # print(df.head())
    if rank == 0:
        ta = df[16434, 9]
        vals = new_head.index.unique()
        for n in vals:
            print(n, new_head.index.get_loc(n))
        # print(dir(new_head.index))
    # if rank==0: print(df['run'])
    return df


if __name__ == "__main__":

    comm_world = MPI.COMM_WORLD
    rank = comm_world.rank
    n_ranks = comm_world.size
    # Capture the start time of the rank here. We do it here to allow us
    # to observe the ragged startup times.

    # Barrier 1: all ranks have started

    debug_out = open("info" + str(rank) + ".txt", "w")

    comm_world.Barrier()
    if len(sys.argv) != 2:
        print_one(rank, "Please specify exactly 1 input file")
        sys.exit(1)
    infile = sys.argv.pop()

    # Each rank opens HDF file, and processes it.
    with File(infile, "r", driver="mpio", comm=comm_world) as input_file:
        who_owns_what = get_subruns_index(rank, n_ranks, input_file["hdr"], "hdr")

        # if this was a numpt 2D array ..
        # i_own_these = who_owns_what[who_owns_what[:,0]==rank]

        i_own_these = who_owns_what.loc[who_owns_what["rank"] == rank]
        # if rank==0: print(i_own_these)

        # returns a dictionary of table_name -> data_frame
        tables = process_file(input_file, rank, n_ranks, i_own_these)
        # if rank==0: print(tables['vtx'])

        # numpy array of bools, one per slice in the tables attached in the function
        my_answers = kNueSecondAnaDQ.filter(tables)
        print("Who passes? ", np.nonzero(my_answers), rank, len(my_answers))
        local_sum = np.array(np.sum(my_answers), "d")
        global_sum = np.array(0.0, "d")
        comm_world.Reduce(local_sum, global_sum, op=MPI.SUM, root=0)

        # print(my_answers)
        print("How many passed? ", local_sum, global_sum)
