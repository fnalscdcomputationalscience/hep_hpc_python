from __future__ import print_function
import time
# from builtins import range
start_time = time.time()
import sys
for p in sys.path:
	print(p)

class Error(Exception):
    def __init__(self, msg):
        super(Error, self).__init__(msg)

try:
    import mpi4py
    from mpi4py import MPI
except:
    raise Error("mpi4py not found; maybe set up the required UPS products?")
from h5py import File
import numpy as np
from mpi_numerology import calculate_slice_for_rank
import argparse

# Magic numbers for the LArIAT detector
WIRES_PER_PLANE = 240
SAMPLES_PER_WIRE = 3072
BYTES_PER_SAMPLE = 2

# The size (in bytes) of each LArIAT ADC array object (for 1 view, in 1 event).
WIRES_SIZE_PER_ENTRY = WIRES_PER_PLANE * SAMPLES_PER_WIRE * BYTES_PER_SAMPLE

# Number of bytes in a GiB
GIGABYTE = 1024 ** 3

# The maximum amount of memory the program as a whole (summed over all ranks)
# will be allowed to read at one time.
MAX_PROGRAM_MEMORY = 64 * GIGABYTE

FILTER_FFT = np.ones((SAMPLES_PER_WIRE // 2) + 1, complex)

timing_data = []


def parse_args():
    parser = argparse.ArgumentParser(description="Process the LArIAT raw digit data.")
    parser.add_argument(
        "--mem-max",
        "-m",
        help="Maximum memory usable by the *whole program* for wire processing (GiB).",
        type=int,
        default=96,
    )
    parser.add_argument(
        "--nevt", "-n", help="Maximum number of events to process", type=int
    )
    parser.add_argument(
        "--timing-file",
        "-t",
        help="Name of timing data output file.",
        default="timing.dat",
    )
    parser.add_argument("input", help="Files to read", nargs="+")
    return parser.parse_args()


def record_time(rank, stage_label, **kwargs):
    timing_data.append(
        [
            rank,
            stage_label,
            MPI.Wtime(),
            kwargs["iter"] if kwargs is not None and "iter" in kwargs else -1,
            kwargs["nevt"] if kwargs is not None and "nevt" in kwargs else -1,
        ]
    )


def print_one(r, msg):
    """Print the given string from only one rank (rank 0)."""
    if r == 0:
        print(msg)


# @profile
def process_file(infile, rank, n_ranks, n_entries_remaining):
    """Process the given HDF5 File object with LArIAT-like waveform processing.

    :param infile: an open HDF5 File object with LArIAT waveform data.
    :param rank: the rank of the current process
    :param n_ranks: the total number of ranks in the process
    :return: None
    """

    # wavefile = open("wave.txt", "w")
    # rawfile = open("raw.txt", "w")
    wavefile = None
    rawfile = None
    # Each rank finds its dataset to read. Is there a problem with
    # some reading from one group, and others reading from a different
    # group? I don't think there is.
    u_group = infile["/rawDigits_u"]
    u_adcdata = u_group["adc"]
    n_entries_to_read = len(u_adcdata)
    if n_entries_remaining is not None:
        n_entries_to_read = min(n_entries_remaining, n_entries_to_read)

    v_group = infile["/rawDigits_v"]
    v_adcdata = v_group["adc"]
    # if len(v_adcdata) != n_entries:
    #    raise Error("Data file %s is corrupt. rawDigits_u claims %d events, rawDigits_v claims %d events" % (
    #        infile.filename, n_entries, len(v_adcdata)))

    # print_one(rank, "We have %d events\n" % n_entries)
    # We split the data so that the first half of the ranks process the
    # u-view, and the second half process the v-view. We define effective_rank
    # to run from [0... ranks_per_view) for each of the views.
    if n_ranks != 1 and n_ranks % 2 != 0:
        raise Error("Must use 1 rank or an even number of ranks")
    if n_ranks == 1:
        ranks_per_view = 1
        my_effective_rank = 0
        print("Running with only 1 rank: only U view will be processed")
    else:
        ranks_per_view = n_ranks // 2
        my_effective_rank = rank % ranks_per_view
    if rank < ranks_per_view:
        my_adcdata = u_adcdata
    else:
        my_adcdata = v_adcdata

    record_time(rank, "BeforeCalculateSlice", nevt=n_entries_to_read)

    # This rank will read its view, for entries [my_slice_start, my_slice_stop)
    my_slice_start, my_slice_stop = calculate_slice_for_rank(
        my_effective_rank, ranks_per_view, n_entries_to_read
    )
    rows_to_read_for_rank = my_slice_stop - my_slice_start

    record_time(rank, "AfterCalculateSlice", nevt=rows_to_read_for_rank)

    my_memory_limit = (
        MAX_PROGRAM_MEMORY // n_ranks
    )  # most memory one rank is allowed to use.
    # The multiplier below is the peak (guess!) at the number of bytes/sample for the program.
    max_rows_per_iteration = int(my_memory_limit // (WIRES_SIZE_PER_ENTRY * 23))
    print_one(rank, "We will read %d rows per iteration\n" % max_rows_per_iteration)
    print_one(rank, "my_memory_limit is: %d\n" % my_memory_limit)

    a, b = divmod(rows_to_read_for_rank, max_rows_per_iteration)
    num_iterations = int(a if b == 0 else a + 1)

    record_time(rank, "BeforeIterations", nevt=rows_to_read_for_rank)

    for i in range(0, num_iterations):
        process_many_events(
            i,
            max_rows_per_iteration,
            my_adcdata,
            my_slice_start,
            my_slice_stop,
            rank,
            rawfile,
            wavefile,
        )

    if wavefile:
        wavefile.close()
    if rawfile:
        rawfile.close()

    return n_entries_to_read


# @profile
def process_many_events(
    i,
    max_rows_per_iteration,
    my_adcdata,
    my_slice_start,
    my_slice_stop,
    rank,
    rawfile,
    wavefile,
):
    record_time(rank, "IterationStart", iter=i)
    this_iteration_first = my_slice_start + i * max_rows_per_iteration
    this_iteration_last = min(
        this_iteration_first + max_rows_per_iteration, my_slice_stop
    )
    # adc_data: 2 bytes/sample
    record_time(rank, "BeforeDataRead", iter=i)
    adc_data = my_adcdata[this_iteration_first:this_iteration_last]
    record_time(rank, "AfterDataRead", iter=i)
    events_this_time = len(adc_data)
    #    print_one(rank, "events_this_time: %d" % events_this_time)
    # adc_floats: 8 bytes/sample
    adc_floats = adc_data.astype(float)
    adc_floats.shape = (events_this_time * WIRES_PER_PLANE, SAMPLES_PER_WIRE)
    # waveforms: 8 bytes/sample
    waveforms = transform_wires(adc_floats)
    if rawfile and wavefile:
        write_interesting_wires(adc_floats, rank, rawfile, wavefile, waveforms)
    waveforms.shape = (events_this_time, WIRES_PER_PLANE, SAMPLES_PER_WIRE)
    # We don't actually do anything with the resulting waveforms... the next step would be
    # to do hit finding on each wire.
    record_time(rank, "IterationEnd", iter=i, nevt=events_this_time)


# @profile
def transform_wires(adc_floats):
    """
    Calculate the noise-reduced waveform, following the techniqued used in LArSoft.
    :rtype: nd.array
    """
    # ftrans: 16 bytes/value, (n samples/2) + 1 values; ~8 bytes/sample
    ftrans = np.fft.rfft(adc_floats, axis=1)
    # ftrans_mags: ~8 bytes/sample
    ftrans_mags = np.absolute(ftrans)
    # thresholds: 8 bytes/wire (negligible)
    thresholds = np.amax(ftrans_mags, axis=1) / 50
    thresholds.shape = (adc_floats.shape[0], 1)
    # This is complicated, because of temporaries...
    # The worst case:
    #   logical array: 1 byte/sample
    #   float array resulting from logical*float multiplication: 8 byte/sample
    #   float array of inverse transform: 8 byte/sample
    return np.fft.irfft(1 * (ftrans_mags > thresholds) * ftrans, axis=1)


def write_interesting_wires(adc_floats, rank, rawfile, wavefile, waveforms):
    if rank != 0:
        return
    interesting_wires = np.any(np.abs(waveforms) > 50, axis=1)
    interesting_wires.shape = (len(interesting_wires), 1)
    if not np.any(interesting_wires):
        return
    for idx in range(0, len(interesting_wires)):
        write_wire_if_interesting(
            adc_floats, idx, interesting_wires[idx], rawfile, wavefile, waveforms
        )


def write_wire_if_interesting(
    adc_floats, idx, interesting_wire, rawfile, wavefile, waveforms
):
    if interesting_wire:
        np.savetxt(rawfile, adc_floats[idx][None])
        np.savetxt(
            wavefile, waveforms[idx][None]
        )  # The None is needed to get output on one line


if __name__ == "__main__":
    main_time = time.time()
    print(main_time - start_time)
    comm_world = MPI.COMM_WORLD
    rank = comm_world.rank
    n_ranks = comm_world.size

    # Capture the start time of the rank here. We do it here to allow us
    # to observe the ragged startup times.
    record_time(rank, "BeforeStartBarrier")

    # Barrier 1: all ranks have started
    comm_world.Barrier()
    record_time(rank, "AfterStartBarrier")

    # Handle arguments.
    args = parse_args()
    MAX_PROGRAM_MEMORY = args.mem_max * GIGABYTE

    # Each rank opens HDF file, and processes it.
    record_time(rank, "BeforeProcessFiles", nevt=args.nevt or -1)
    for file_iter, infile in enumerate(args.input):
        record_time(rank, "BeforeProcessFile", iter=file_iter, nevt=args.nevt or -1)
        with File(infile, "r", driver="mpio", comm=comm_world) as input_file:
            n_entries_read = process_file(input_file, rank, n_ranks, args.nevt)
            if args.nevt is not None:
                args.nevt -= n_entries_read
                if args.nevt <= 0:
                    break
        record_time(rank, "AfterProcessFile", iter=file_iter, nevt=args.nevt or -1)
    record_time(rank, "AfterProcessFiles")

    all_timing_data = comm_world.reduce(timing_data, op=MPI.SUM, root=0)
    record_time(rank, "AfterReduce")

    if rank == 0:
        time_label = (
            "GlobalTime"
            if comm_world.Get_attr(MPI.WTIME_IS_GLOBAL) == 1
            else "LocalTime"
        )
        with open(args.timing_file, "w") as output_file:
            output_file.write("Rank\tStage\t{0}\tIter\tNevt\n".format(time_label))
            for x in all_timing_data:
                output_file.write("{0}\t{1}\t{2}\t{3}\t{4}\n".format(*x))
            output_file.write(
                "{0}\t{1}\t{2}\t-1\t-1\n".format(rank, "AfterTimingDump", MPI.Wtime())
            )
