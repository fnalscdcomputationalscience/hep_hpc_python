########################################################################
# This GNUmakefile requires UPS product ssddoc v1_09 or better for
# knitting and font embedding: setup the UPS product or ensure an
# up-to-date copy of the cet-is repository in parent.
#
# To make a PDF file pyhpc-18-final.pdf with embedded fonts, simply:
#
#   make final
#
# To make the .tex file for debugging purposes, either make it
# explicitly:
#
#   make pyhpc-18.tex
#
# OR uncomment the definition of the .SECONDARY special target below.
# You may then wish to add pyhpc-18.tex to the clean_files variable
#
# 2018-10-24 CG.
########################################################################
TALK=Python_and_HPC_for_High_Energy_Physics_Data_Analyses

# Things we wish to build. Note that we do *not* mention $(TALK).pdf
# here because it is not *built* in the normal way.
PRODUCTS = pyhpc-18.pdf pyhpc-18-talk.pdf

# Build $(TALK).pdf as part of the all target.
ALL = $(PRODUCTS) $(TALK).pdf

####################################
# Preamble.

# Need to find latex.mk somewhere.
ifneq (,$(SSDDOC_INC))
  ldir = $(SSDDOC_INC)
else
  ldir = ../../cet-is/ssddoc/include
endif

# Include it.
include $(ldir)/latex.mk

# Check version.
ifneq (,$(LATEX_MK_VERSION_NUM))
  latex_mk_version_ok := $(shell (($(LATEX_MK_VERSION_NUM) >= 20101)) && echo OK)
endif

ifeq (,$(latex_mk_version_ok))
  $(error "ssdoc >= v2_01_01 required.")
endif
####################################

# On with the show.
export TEXINPUTS=$(LATEX_MK_DIR)//:

# Extra cleanup from KnitR.
override clean_files += $(foreach d,$(PRODUCTS),$(d:.pdf=-tikzDictionary))

# Instructions to generate $(TALK).pdf as a final document for
# submission with embedded fonts and a nice filename.
.INTERMEDIATE : pyhpc-18-talk-packed.pdf
$(TALK).pdf : pyhpc-18-talk-packed.pdf
	$(ECHO) "Renaming $(<) to $(@)."
	$(AT)mv -v $(<) $(@) $(QUIET_REDIRECT)

override clobber_files += $(TALK).pdf

# Uncomment to keep the generated .tex file around (until we clean or clobber).
#.SECONDARY: pyhpc-18.tex
#override clean_files += pyhpc-18.tex

# Regenerate and clean up .bbl files.
override LATEXMK_EXTRA_OPTS += -bibtex
