$ ipython
Python 3.7.0 (default, Jun 29 2018, 20:13:13)
Type 'copyright', 'credits' or 'license' for more information
IPython 6.5.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: x = 23128015011840

In [2]: x / 1024**3
Out[2]: 21539.642486572266

In [3]: x / 1024**4
Out[3]: 21.034807115793228

In [4]: y = 7528650720 + 15057301440 + 15057301440 + 7528650720 + 172531579

In [5]: x/(x+y)
Out[5]: 0.9980432515190013

In [6]: a = 2247315039464

In [7]: b = 133768636 + 27410434 + 10161655891 + 232063484 + 864754

In [8]: a/(a+b)
Out[8]: 0.9953249038047038
