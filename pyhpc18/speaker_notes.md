---
title: Data-parallel Python for High Energy Physics Analyses
subtitle: Speaker's notes
author: Marc Paterno
---

# Introduction

This question is of growing importance, especially to US HEP,
because the DOE is guiding HEP to the use of DOE-funded supercomputing sites.

# What is a neutrino?

Neutrinos are the lightest matter particles,
but they are very hard to detect.

This picture is of Supernova 1987A,
which exploded,
producing an estimated 10^58 neutrinos.
20 were detected worldwide, by 3 different detectors.

Flux is 7 * 10^10 / cm2 neutrinos from the sun going through you every second.

# Neutrino oscillations

Neutrinos are the least-well-understood particles in nature.

They carry no electric charge, and interact only very weakly with other matter.

They come in three types, called flavors,
which correspond to the charged particles to which they are related.

They are very light,
and typically travel a nearly the speed of light.

Most strangely,
a neutrino of one type can turn into one of another type.

This is called oscillation.

# Neutrino detectors

Because neutrinos interact so weakly,
neutrino detectors have to be large.

The next big neutrino detector is being built in South Dakota,
a mile underground.

Detectors like this are best placed underground,
to shield them from cosmic rays --- particles from space that pollute the sparse neutrino interaction data.

# Liquid Argon Time Projection Chambers

A time projection chamber is a type of particle detector that uses
electric and possibly magnetic fields together with a sensitive volume of
gas or liquid to identify charged particle trajectories and interactions
through ionization.

# The common noise reduction technique

Signals collected by detector electronics include many sources of noise.
These include:

* movement and impurities of the LAr
* electromagnetic interference from power supplies and other equipment in the facility
* vibrations from the environment.

# Setup for performance measurements

To characterize the performance and scalability of our code,
we performed ran several experiments
(limited by our available project resources at NERSC).

# Scaling of processing speed for different Burst Buffer allocations

Note that the 5 TiB BB allocation has 6, 3, and 6 measurements at each point;
the speed are so reproducible that on this scale no separation appears.

# Reading and decompressing Data

Part of the reason we are not hitting any IO limitation may be
that we are reading compressed data,
and we cannot disentangle the time spent reading from that spent decompressing.
