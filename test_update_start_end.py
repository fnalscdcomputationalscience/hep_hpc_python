import unittest
import pandas as pd
from update_start_end import update_start_end

df = pd.read_csv("sample.txt")

class TestAdjustStartEnd(unittest.TestCase):
    def test_overlap_is_se_split(self):
        
    def test_overlap_is_gt_split(self):
        self.assertEqual(update_start_end(df, 0, 3, 3), (0, 0, 7))
        self.assertEqual(update_start_end(df, 1, 3, 3), (1, 7, 12))
        self.assertEqual(update_start_end(df, 2, 3, 3), (2, 12, 17))

    def test_no_split_among_ranks(self):
        self.assertEqual(update_start_end(df, 0, 2, 3), (0, 0, 9))
        self.assertEqual(update_start_end(df, 1, 2, 3), (1, 9, 17))

    def test_no_split_one_rank(self):
        self.assertEqual(update_start_end(df, 0, 1, 3), (0, 0, 17))

if __name__ == '__main__':
    unittest.main()
