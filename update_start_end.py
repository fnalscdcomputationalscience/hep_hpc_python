from mpi4py import MPI
import mpi_numerology as mpinum
import numpy as np
import os
import pandas as pd
import sys


def update_end(df, overlap, end):
    d = df[end - 1 : overlap + end].diff().dropna(how="all")
    val = (d[0:1] != 0).any(axis=1).values[0]
    if ~val:
        end = d.loc[~(d == 0).all(axis=1)].index.values[0]
    return end


def update_start(df, overlap, start):
    # beginning from the start to overlap, find the rows that are
    # different from the last one and drop the NAs
    # Find all the rows that are non-zero because the first non-zero
    # row index is the new start
    d1 = df[start - 1 : start + 1].diff().dropna(how="all")
    check = (d1[0:1] != 0).any(axis=1).values[0]
    if ~check:
        d = df[start : start + overlap].diff().dropna(how="all")
        if ~d.all().all():
            start = d.loc[~(d == 0).all(axis=1)].index.values[0]
    return start


def update_start_end(df, myrank, nranks, overlap):
    arraysz = len(df.index)
    start, end = mpinum.calculate_slice_for_rank(myrank, nranks, arraysz)
    if myrank != nranks - 1:
        end = update_end(df, overlap, end)
    if myrank != 0:
        start = update_start(df, overlap, start)
    return myrank, start, end
